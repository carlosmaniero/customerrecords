# Customer Records

This is a Java 9 application, my main goal with this application was to use Java 9 modules. So the application is 
distributed in four modules:

## app module
It is basically the text-based ui that the user interacts with.

## geolocation module
Abstracts the complexity of the [Great Circle Distance](https://en.wikipedia.org/wiki/Great-circle_distance) algorithm
by exporting only `GeographicCoordinate` class. It was nice to have Java modules in this case because the geolocation
module is not part of the application domain. It is just a dependency to solve the problem.

## company module
This modules contains information about the company. More precisely, it has just the Company geolocation. As 
`geolocation` module it isn't within the boundaries of the customers application neither.

## customers module
This is the module that really solve the problem. It contains the `json` parser and the application service that check
witch customers could be invited to the party.

# Running

There is some over tooling at the build, you can just use the gradle, otherwise I just created a 
[Dockerfile](./Dockerfile) that can be easily executed through a [Makefile](./Makefile).

```bash
# Building
make
# Testing
make test
# Running
make run < some/file.txt
```

## Demo file

```bash
make run < app/src/test/resources/customers.txt
```
