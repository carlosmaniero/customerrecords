default:
	@docker build -t customer-records .

run:
	@docker run -i --rm --name customer-records customer-records

test:
	@docker run -i --rm --name customer-records customer-records ./gradlew test