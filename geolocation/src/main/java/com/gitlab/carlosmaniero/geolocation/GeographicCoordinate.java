package com.gitlab.carlosmaniero.geolocation;

import java.util.Objects;

import static com.gitlab.carlosmaniero.geolocation.distance_calculator.EarthDistanceCalculator.calculateDistanceBetween;

public class GeographicCoordinate {
    private final double latitude;
    private final double longitude;

    public GeographicCoordinate(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public double distanceBetween(GeographicCoordinate anotherCoordinate) {
        return calculateDistanceBetween(
                this.getLatitude(), this.getLongitude(),
                anotherCoordinate.getLatitude(), anotherCoordinate.getLongitude()
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GeographicCoordinate that = (GeographicCoordinate) o;
        return Double.compare(that.latitude, latitude) == 0 &&
                Double.compare(that.longitude, longitude) == 0;
    }

    @Override
    public String toString() {
        return "GeographicCoordinate{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(latitude, longitude);
    }
}
