package com.gitlab.carlosmaniero.geolocation.distance_calculator;

import static java.lang.Math.*;

public class EarthDistanceCalculator {
    private static final int EARTH_RADIUS = 6371;

    public static double calculateDistanceBetween(
            double degreesInitialLat, double degreesInitialLong, double degreesFinalLat, double degreesFinalLong) {
        return EARTH_RADIUS * calculateCentralAngleInRadians(
                degreesInitialLat, degreesInitialLong, degreesFinalLat, degreesFinalLong);
    }

    private static double calculateCentralAngleInRadians(
            double degreesInitialLat, double degreesInitialLong, double degreesFinalLat, double degreesFinalLong) {
        final double initialLat = toRadians(degreesInitialLat);
        final double initialLong = toRadians(degreesInitialLong);
        final double finalLat = toRadians(degreesFinalLat);
        final double finalLong = toRadians(degreesFinalLong);

        final double deltaLong = calculateDelta(initialLong, finalLong);

        return atan(sqrt(
                pow(cos(finalLat) * sin(deltaLong), 2) +
                        pow(cos(initialLat) * sin(finalLat) - sin(initialLat) * cos(finalLat) * cos(deltaLong), 2)
        ) / (sin(initialLat) * sin(finalLat) + cos(initialLat) * cos(finalLat) * cos(deltaLong)));
    }

    private static double calculateDelta(double initialPosition, double finalPosition) {
        return finalPosition - initialPosition;
    }
}
