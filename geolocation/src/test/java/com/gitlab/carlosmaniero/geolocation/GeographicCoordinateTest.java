package com.gitlab.carlosmaniero.geolocation;

import com.gitlab.carlosmaniero.geolocation.GeographicCoordinate;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GeographicCoordinateTest {
    @Test
    public void calculatesDistanceBetweenTwoPoints() {
        final GeographicCoordinate prefeitoSaladinoTrainStation = new GeographicCoordinate(-23.638011, -46.536526);
        final GeographicCoordinate consolacaoSubwayStation = new GeographicCoordinate(-23.557505, -46.660614);

        assertThat(prefeitoSaladinoTrainStation.distanceBetween(consolacaoSubwayStation))
                .isBetween(15.0, 16.0);
    }
}