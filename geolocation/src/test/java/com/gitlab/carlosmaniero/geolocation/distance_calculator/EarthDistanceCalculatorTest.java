package com.gitlab.carlosmaniero.geolocation.distance_calculator;

import org.junit.Test;

import static com.gitlab.carlosmaniero.geolocation.distance_calculator.EarthDistanceCalculator.calculateDistanceBetween;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class EarthDistanceCalculatorTest {
    @Test
    public void returnsZeroWhenCalculatingTheDistanceBetweenTheSamePoint() {
        assertThat(calculateDistanceBetween(32.9697, -96.80322, 32.9697, -96.80322))
                .isEqualTo(0);
    }

    @Test
    public void calculatesTheDistanceBetweenTwoFarPositions() {
        assertThat(calculateDistanceBetween(-33.748162, -53.411163, 3.905666, -51.834048))
                .isBetween(4190.0, 4192.0);
    }

    @Test
    public void calculatesTheDistanceBetweenTwoClosePositions() {
        assertThat(calculateDistanceBetween(-23.638011, -46.536526, -23.557505, -46.660614))
                .isBetween(15.0, 16.0);
    }
}