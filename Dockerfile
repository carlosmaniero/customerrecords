FROM openjdk:9
COPY . /usr/src/app
WORKDIR /usr/src/app
RUN ./gradlew build
CMD ["./gradlew", "app:run", "--quiet"]

