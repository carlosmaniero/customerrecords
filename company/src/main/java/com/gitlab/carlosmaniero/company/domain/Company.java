package com.gitlab.carlosmaniero.company.domain;

import com.gitlab.carlosmaniero.geolocation.GeographicCoordinate;

public class Company {
    private static final GeographicCoordinate OFFICE_GEOGRAPHIC_COORDINATE = new GeographicCoordinate(53.339428, -6.257664);

    public static GeographicCoordinate companyOfficePosition() {
        return OFFICE_GEOGRAPHIC_COORDINATE;
    }
}
