module com.gitlab.carlosmaniero.company {
    exports com.gitlab.carlosmaniero.company.domain;

    requires transitive com.gitlab.carlosmaniero.geolocation;
}