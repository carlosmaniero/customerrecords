package com.gitlab.carlosmaniero.customers.application;

import com.gitlab.carlosmaniero.customers.domain.Customer;
import com.gitlab.carlosmaniero.geolocation.GeographicCoordinate;
import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;

public class InviteServiceTest {
    @Test
    public void returnsAnEmptyListWhenThereIsNoCustomer() {
        assertThat(InviteService.customersCloseToTheOffice(emptyList())).isEmpty();
    }

    @Test
    public void returnsTheListOfUsersThatIsCloseToTheDublinOffice() {
        List<String> customersJson = asList(
            "{\"latitude\": \"52.240382\", \"user_id\": 10, \"name\": \"Georgina Gallagher\", \"longitude\": \"-6.972413\"}\n",
            "{\"latitude\": \"53.2451022\", \"user_id\": 4, \"name\": \"Ian Kehoe\", \"longitude\": \"-6.238335\"}\n",
            "{\"latitude\": \"53.1489345\", \"user_id\": 31, \"name\": \"Alan Behan\", \"longitude\": \"-6.8422408\"}\n",
            "{\"latitude\": \"53\", \"user_id\": 13, \"name\": \"Olive Ahearn\", \"longitude\": \"-7\"}\n",
            "",
            "sdsa",
            "{}",
            "{\"latitude\": \"51.999447\", \"user_id\": 14, \"name\": \"Helen Cahill\", \"longitude\": \"-9.742744\"}\n",
            "{\"latitude\": \"52.966\", \"user_id\": 15, \"name\": \"Michael Ahearn\", \"longitude\": \"-6.463\"}"
        );

        Customer customer1 = new Customer(4, "Ian Kehoe", new GeographicCoordinate(53.2451022, -6.238335));
        Customer customer2 = new Customer(13, "Olive Ahearn", new GeographicCoordinate(53.0, -7));
        Customer customer3 = new Customer(15, "Michael Ahearn", new GeographicCoordinate(52.966, -6.463));
        Customer customer4 = new Customer(31, "Alan Behan", new GeographicCoordinate(53.1489345, -6.8422408));

        assertThat(InviteService.customersCloseToTheOffice(customersJson)).containsExactly(
                customer1, customer2, customer3, customer4
        );
    }
}