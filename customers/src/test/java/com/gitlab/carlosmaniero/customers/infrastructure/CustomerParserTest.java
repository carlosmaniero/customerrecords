package com.gitlab.carlosmaniero.customers.infrastructure;

import com.gitlab.carlosmaniero.customers.domain.Customer;
import com.gitlab.carlosmaniero.geolocation.GeographicCoordinate;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CustomerParserTest {
    @Test
    public void returnsAnEmptyListWhenNoInputIsGiven() {
        assertThat(CustomerParser.parse("")).isEmpty();
    }

    @Test
    public void returnsACustomerGivenAValidCustomerJSON() {
        String givenCustomersJsonList = "" +
                "{\"latitude\": \"53.1302756\", \"user_id\": 5, \"name\": \"Nora Dempsey\", \"longitude\": \"-6.2397222\"}";


        Customer customer = new Customer(5, "Nora Dempsey", new GeographicCoordinate(53.1302756, -6.239722));

        assertThat(CustomerParser.parse(givenCustomersJsonList)).contains(customer);
    }

    @Test
    public void returnsEmptyGivenAnInvalidJson() {
        String givenCustomersJsonList = "this-is-not-a-json";
        assertThat(CustomerParser.parse(givenCustomersJsonList)).isEmpty();
    }

    @Test
    public void returnsEmptyGivenThatRequiredFieldsWereNotProvided() {
        String givenCustomersJsonList = "{}";
        assertThat(CustomerParser.parse(givenCustomersJsonList)).isEmpty();
    }
}