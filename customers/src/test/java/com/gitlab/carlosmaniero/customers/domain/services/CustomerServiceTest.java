package com.gitlab.carlosmaniero.customers.domain.services;

import com.gitlab.carlosmaniero.customers.domain.Customer;
import com.gitlab.carlosmaniero.geolocation.GeographicCoordinate;
import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;

public class CustomerServiceTest {
    @Test
    public void returnsAnEmptyListGivenNoCustomers() {
        List<Customer> givenCustomers = emptyList();
        GeographicCoordinate givenPlace = new GeographicCoordinate(1, 1);

        assertThat(CustomerService.customersCloseThan(givenCustomers, givenPlace).withinTheKilometersRangeOf(100.0))
            .isEmpty();
    }

    @Test
    public void returnsTheCustomerWhenTheyAreAtTheSamePlaceEvenWhenTheRangeDistanceIsZero() {
        GeographicCoordinate givenPlace = new GeographicCoordinate(1, 1);

        Customer givenCustomer1 = new Customer(1, "Ada", givenPlace);
        Customer givenCustomer2 = new Customer(2, "Grace", givenPlace);

        List<Customer> givenCustomers = asList(
                givenCustomer1,
                givenCustomer2
        );

        assertThat(CustomerService.customersCloseThan(givenCustomers, givenPlace).withinTheKilometersRangeOf(0))
                .containsExactly(givenCustomer1, givenCustomer2);
    }


    @Test
    public void filtersCustomersThatAreNotCloseEnough() {
        GeographicCoordinate givenPlace = new GeographicCoordinate(1, 1);

        Customer givenCustomer1 = new Customer(1, "Ada", new GeographicCoordinate(1.1, 1.1));
        Customer givenCustomer2 = new Customer(2, "Grace", new GeographicCoordinate(1.09, 1.09));

        List<Customer> givenCustomers = asList(
                givenCustomer1,
                givenCustomer2
        );

        assertThat(CustomerService.customersCloseThan(givenCustomers, givenPlace).withinTheKilometersRangeOf(15))
                .containsExactly(givenCustomer2);
    }
}