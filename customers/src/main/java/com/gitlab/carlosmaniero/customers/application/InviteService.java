package com.gitlab.carlosmaniero.customers.application;

import com.gitlab.carlosmaniero.company.domain.Company;
import com.gitlab.carlosmaniero.customers.domain.Customer;
import com.gitlab.carlosmaniero.customers.infrastructure.CustomerParser;
import com.gitlab.carlosmaniero.customers.domain.services.CustomerService;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

public class InviteService {
    private static final int MAX_DISTANCE_TO_THE_OFFICE = 100;

    public static List<Customer> customersCloseToTheOffice(List<String> customerJsonInputStream) {
        return CustomerService
                .customersCloseThan(customersFromJson(customerJsonInputStream), Company.companyOfficePosition())
                    .withinTheKilometersRangeOf(MAX_DISTANCE_TO_THE_OFFICE)
                    .stream()
                    .sorted(InviteService::customerSortingComparator)
                    .collect(toList());
    }

    private static int customerSortingComparator(Customer customer1, Customer customer2) {
        return (int) (customer1.getId() - customer2.getId());
    }

    private static List<Customer> customersFromJson(List<String> customerJsonInputStream) {
        return customerJsonInputStream.stream().map(CustomerParser::parse)
                .flatMap(Optional::stream)
                .collect(toList());
    }
}
