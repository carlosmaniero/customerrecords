package com.gitlab.carlosmaniero.customers.infrastructure;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.carlosmaniero.customers.domain.Customer;
import com.gitlab.carlosmaniero.geolocation.GeographicCoordinate;

import java.util.Optional;


public class CustomerParser {
    public static Optional<Customer> parse(String json) {
        ObjectMapper mapper = new ObjectMapper();

        try {
            return Optional.of(mapper.readValue(json, CustomerJson.class).toCustomer());
        } catch (Exception e) {
            return Optional.empty();
        }
    }

    private static class CustomerJson {
        private final long id;
        private final String name;
        private final double latitude;
        private final double longitude;

        @JsonCreator
        private CustomerJson(
                @JsonProperty(required = true, value = "user_id") long id,
                @JsonProperty(required = true, value = "name") String name,
                @JsonProperty(required = true, value = "latitude") double latitude,
                @JsonProperty(required = true, value = "longitude") double longitude
        ) {
            this.id = id;
            this.name = name;
            this.latitude = latitude;
            this.longitude = longitude;
        }


        public Customer toCustomer() {
            return new Customer(this.id, this.name, new GeographicCoordinate(this.latitude, this.longitude));
        }
    }
}
