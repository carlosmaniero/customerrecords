package com.gitlab.carlosmaniero.customers.domain;

import com.gitlab.carlosmaniero.geolocation.GeographicCoordinate;

import java.util.Objects;

public class Customer {
    private final long id;
    private final String name;
    private final GeographicCoordinate position;

    public Customer(long id, String name, GeographicCoordinate position) {
        this.id = id;
        this.name = name;
        this.position = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return id == customer.id &&
                Objects.equals(name, customer.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public GeographicCoordinate getPosition() {
        return position;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", position=" + position +
                '}';
    }
}
