package com.gitlab.carlosmaniero.customers.domain.services;

import com.gitlab.carlosmaniero.customers.domain.Customer;
import com.gitlab.carlosmaniero.geolocation.GeographicCoordinate;

import java.util.List;

import static java.util.stream.Collectors.toList;

public class CustomerService {
    public static CustomersInRange customersCloseThan(List<Customer> customers, GeographicCoordinate place) {
        return new CustomersInRange(customers, place);
    }

    public static class CustomersInRange {
        private final List<Customer> customers;
        private final GeographicCoordinate place;


        private CustomersInRange(List<Customer> customers, GeographicCoordinate place) {
            this.customers = customers;
            this.place = place;
        }

        public List<Customer> withinTheKilometersRangeOf(double distance) {
            return this.customers.stream()
                    .filter(customer -> isClientCloseEnough(distance, customer))
                    .collect(toList());
        }

        private boolean isClientCloseEnough(double distance, Customer customer) {
            return customer.getPosition().distanceBetween(this.place) <= distance;
        }
    }
}
