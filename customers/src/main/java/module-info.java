module com.gitlab.carlosmaniero.customers {
    exports com.gitlab.carlosmaniero.customers.domain;
    exports com.gitlab.carlosmaniero.customers.application;
    requires transitive com.gitlab.carlosmaniero.geolocation;
    requires transitive com.gitlab.carlosmaniero.company;

    requires jackson.databind;
    requires jackson.annotations;
    requires jackson.module.jsonSchema;
    opens com.gitlab.carlosmaniero.customers.infrastructure to jackson.databind,jackson.module.jsonSchema;
}