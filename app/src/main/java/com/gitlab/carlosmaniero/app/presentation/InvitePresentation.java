package com.gitlab.carlosmaniero.app.presentation;

import com.gitlab.carlosmaniero.customers.domain.Customer;

public class InvitePresentation {
    public static String errorInputNotProvided() {
        return "You must provide an input";
    }

    public static String customerPresentation(Customer customer) {
        return String.format("%4s - %s", customer.getId(), customer.getName());
    }
}
