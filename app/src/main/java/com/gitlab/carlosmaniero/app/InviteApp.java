package com.gitlab.carlosmaniero.app;

import com.gitlab.carlosmaniero.app.presentation.InvitePresentation;
import com.gitlab.carlosmaniero.customers.application.InviteService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

class InviteApp {
    static void execute() {
        List<String> inputLines = getInitialInput();

        if (inputLines.isEmpty()) {
            System.err.println(InvitePresentation.errorInputNotProvided());
            return;
        }

        InviteService
                .customersCloseToTheOffice(inputLines)
                .stream()
                .map(InvitePresentation::customerPresentation)
                .forEach(System.out::println);
    }

    private static List<String> getInitialInput() {
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));

        try {
            if(stdin.ready()) {
                return stdin.lines().collect(toList());
            }
        } catch (IOException ignored) {}

        return emptyList();
    }
}
