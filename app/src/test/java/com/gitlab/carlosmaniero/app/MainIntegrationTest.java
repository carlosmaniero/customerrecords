package com.gitlab.carlosmaniero.app;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;
import java.io.PrintStream;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class MainIntegrationTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;
    private final InputStream originalStdin = System.in;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
        System.setIn(originalStdin);
    }

    @Test
    public void showsAnErrorWhenInputProvided() {
        Main.main(new String[0]);
        assertThat(errContent.toString())
                .startsWith("You must provide an input");
    }

    @Test
    public void doesNotShowAnyErrorWhenAValidFileWasProvided() throws FileNotFoundException {
        mockStdin();
        Main.main(new String[0]);

        assertThat(errContent.toString())
                .isEmpty();
    }

    @Test
    public void showsTheListOfValidUsers() throws FileNotFoundException {
        mockStdin();
        Main.main(new String[0]);

        assertThat(outContent.toString())
                .isEqualTo(
                        "   4 - Ian Kehoe\n" +
                        "   5 - Nora Dempsey\n" +
                        "   6 - Theresa Enright\n" +
                        "   8 - Eoin Ahearn\n" +
                        "  11 - Richard Finnegan\n" +
                        "  12 - Christina McArdle\n" +
                        "  13 - Olive Ahearn\n" +
                        "  15 - Michael Ahearn\n" +
                        "  17 - Patricia Cahill\n" +
                        "  23 - Eoin Gallagher\n" +
                        "  24 - Rose Enright\n" +
                        "  26 - Stephen McArdle\n" +
                        "  29 - Oliver Ahearn\n" +
                        "  30 - Nick Enright\n" +
                        "  31 - Alan Behan\n" +
                        "  39 - Lisa Ahearn\n");
    }

    private void mockStdin() throws FileNotFoundException {
        File file = new File("src/test/resources/customers.txt");

        BufferedReader reader = new BufferedReader(new FileReader(file.getAbsoluteFile()));
        String fileContent = String.join("\n", reader.lines().collect(toList()));
        System.setIn(new ByteArrayInputStream(fileContent.getBytes()));
    }
}